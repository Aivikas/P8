﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loom
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom l1 = new Loom("krokodill");
            l1.TeeHäält();

            KoduLoom k1 = new KoduLoom("Lehm");

            //Kass miisu = new Kass("Miisu",)
        }
            
    }
    class Loom
    {
        public static List<Loom> Loomad = new List<Loom>();

        public string Liik;
        public Loom(string liik = "tundmatu")
        {
            Liik = liik;
            Loomad.Add(this);
        }
        public virtual void TeeHäält()
        {
            Console.WriteLine($"Loom liigist {Liik} teeb hirmsat häält");
        }
        public override string ToString()
        {
            return $"Meil on loom {Liik}";

        }


    }
    class KoduLoom : Loom // on ka loom, aga natuke teistsugune KLASSIDE TULETAMINE
    {
        public string Tõug; // tal on nii Liik kui Tõug

        public KoduLoom(string liik, string tõug = "") : base(liik) // konstruktorite sidumine kooloniga. sarnane sidumisviis on .this
        {
            Tõug = tõug;
            
        }
        public override void TeeHäält()
        {
            Console.WriteLine($"Koduloom {Liik} teeb mahedat häält");

        }
    
    }
    //class Kass : KoduLoom
    //{
    //    public string Nimi;
    //  public kass(string nimi, string tõug = "tundmatu") : base("kass", tõug"siiami")
    //}
}
