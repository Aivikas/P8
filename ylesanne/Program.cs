﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ylesanne
{
    class Program
    {
        static void Main(string[] args)

        { 
            Inimene i1 = new Inimene
            {
                Isikukood = "47708030241",
                EesNimi = "Aivi",
                PereNimi = "Murd"
            };

            Console.WriteLine(i1);

            Opilane o1 = new Opilane
            {
                EesNimi = "Elis Ester",
                PereNimi = "Suurna",
                Klass = "2D"
            };

            Console.WriteLine("$Inimene {i1} on {o1} klassi õpilane");

        }
    }
}
