﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hommik
{
    class Inimene
    {
        string _Isikukood;
        //public readonly string Isikukood <- toimib ainult konstruktoris
        string _Eesnimi;
        string _Perenimi;

        //public Inimene(string isikukood) => _Isikukood = isikukood; // konstruktor


        public Inimene(string isikukood, string eesnimi, string perenimi = "") // teine konstruktor - perenimi on optional parameetere
        {
            _Isikukood = isikukood;
            _Eesnimi = eesnimi;
            _Perenimi = perenimi;
        }
        public string Eesnimi
        {
            get => _Eesnimi;
            set  { _Eesnimi = Suurtäht(value); }
        }
        public string Perenimi
        {
            get => _Perenimi;
            set { _Perenimi = Suurtäht(value); }
        }
        public string Isikukood // read only property
        {
            get { return _Isikukood; }

        }

        public Sugu Sugu => (Sugu)(Isikukood[0] % 2);

        public DateTime Sünniaeg => new DateTime(
            ((_Isikukood[0] - '1') / 2 + 18) * 100 + int.Parse(_Isikukood.Substring(1, 2)),
                int.Parse(_Isikukood.Substring ( 3, 2)),
                int.Parse(_Isikukood.Substring(5, 2))
            );

        public int Vanus =>
            DateTime.Now.Year - Sünniaeg.Year
            + (DateTime.Now.DayOfYear > Sünniaeg.DayOfYear ? 0 : -1);


        public int Vanus2 => (int)((DateTime.Now.Date - Sünniaeg).TotalDays / 365.25);
        
            static string Suurtäht(string s)
            => s.Length == 0 ? s :
            s.Substring(0, 1).ToUpper() + s.Substring(1).ToLower();
    }
}
